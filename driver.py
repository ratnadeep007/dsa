from maximal_square.maximal_square_recursive_dynamic import Solution

matrix = [
    ["1", "0", "1", "0", "0"],
    ["1", "0", "1", "1", "1"],
    ["1", "1", "1", "1", "1"],
    ["1", "0", "0", "1", "0"],
]
sol = Solution()
print(sol.maximalSquare(matrix))
