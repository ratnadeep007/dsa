# Problem: https://leetcode.com/problems/maximal-square/

from typing import List


class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        res = 0
        row = len(matrix)
        if row == 0:
            return res
        col = len(matrix[0])
        cache = [[0 for y in range(col)] for z in range(row)]

        for i in range(row):
            for j in range(col):
                if matrix[i][j] == "1":
                    cache[i][j] = 1
                    res = 1

        for i in range(1, row):
            for j in range(1, col):
                if cache[i][j] != 0:
                    cache[i][j] = 1 + min(
                        min(cache[i - 1][j], cache[i - 1][j - 1]), cache[i][j - 1]
                    )
                    if res < cache[i][j]:
                        res = cache[i][j]

        return res * res
