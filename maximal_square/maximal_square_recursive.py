# Problem: https://leetcode.com/problems/maximal-square/

from typing import List


class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        self.matrix = matrix
        self.ROWS, self.COLS = len(matrix), len(matrix[0]) if matrix else 0
        self.cache = {}  # map each (row, col) -> maxLength
        self.helper(0, 0)
        return max(self.cache.values()) ** 2

    def helper(self, r, c):
        if r >= self.ROWS or c >= self.COLS:
            return 0
        if (r, c) not in self.cache:
            down = self.helper(r + 1, c)
            right = self.helper(r, c + 1)
            diag = self.helper(r + 1, c + 1)

            self.cache[(r, c)] = 0

            if self.matrix[r][c] == "1":
                self.cache[(r, c)] = 1 + min(down, right, diag)

        return self.cache[(r, c)]
