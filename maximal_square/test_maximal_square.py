import unittest

from maximal_square.maximal_square_recursive import Solution as SolR
from maximal_square.maximal_square_recursive_dynamic import Solution as SolD


class TestMaximalSquare(unittest.TestCase):
    def setUp(self) -> None:
        self.matrix = [
            ["1", "0", "1", "0", "0"],
            ["1", "0", "1", "1", "1"],
            ["1", "1", "1", "1", "1"],
            ["1", "0", "0", "1", "0"],
        ]

    def test_recursive(self):
        sol = SolR()
        self.assertEqual(sol.maximalSquare(self.matrix), 4)

    def test_dynamic(self):
        sol = SolD()
        self.assertEqual(sol.maximalSquare(self.matrix), 4)
