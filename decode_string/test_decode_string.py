import unittest

from decode_string.decode_string import Solution


class TestDecodeString(unittest.TestCase):
    def setUp(self) -> None:
        self.string = "3[a]2[bc]"

    def test_decode(self):
        sol = Solution()
        self.assertEqual(sol.decodeString(self.string), "aaabcbc")
