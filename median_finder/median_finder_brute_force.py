# Problem: https://leetcode.com/problems/find-median-from-data-stream/


class MedianFinder:
    def __init__(self):
        self.list_nums = []

    def addNum(self, num: int) -> None:
        if len(self.list_nums) == 0:
            self.list_nums.append(num)
        else:
            for i in range(len(self.list_nums)):
                if self.list_nums[i] > num:
                    self.list_nums.insert(i, num)
                    break
            else:
                self.list_nums.append(num)

    def findMedian(self) -> float:
        len_list = len(self.list_nums)
        if len_list % 2 == 0:
            return (
                self.list_nums[len_list // 2] + self.list_nums[len_list // 2 - 1]
            ) / 2
        else:
            return self.list_nums[len_list // 2]
