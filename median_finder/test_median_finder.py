import pytest

from median_finder.median_finder_brute_force import (
    MedianFinder as MedianFinderBruteForce,
)
from median_finder.median_finder_heap import MedianFinder as MedianFinderHeap


def test_brute_force_method():
    obj = MedianFinderBruteForce()
    obj.addNum(1)
    obj.addNum(2)
    res = obj.findMedian()
    assert res == 1.5
    obj.addNum(3)
    res = obj.findMedian()
    assert res == 2


def test_brute_heap_method():
    obj = MedianFinderHeap()
    obj.addNum(1)
    obj.addNum(2)
    res = obj.findMedian()
    assert res == 1.5
    obj.addNum(3)
    res = obj.findMedian()
    assert res == 2
