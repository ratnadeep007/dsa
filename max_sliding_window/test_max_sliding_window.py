import unittest
from max_sliding_window.max_sliding_window_brute_force import (
    Solution as SolutionBruteForce,
)
from max_sliding_window.max_sliding_window_deque import (
    Solution as SolutionDeque,
)


class TestMaxSlidingWindow(unittest.TestCase):
    def setUp(self) -> None:
        self.nums = [1, 3, -1, -3, 5, 3, 6, 7]
        self.k = 3

    def test_brute_force(self):
        sol = SolutionBruteForce()
        res = sol.maxSlidingWindow(self.nums, self.k)
        expected = [3, 3, 5, 5, 6, 7]
        self.assertListEqual(res, expected)

    def test_deque(self):
        sol = SolutionDeque()
        res = sol.maxSlidingWindow(self.nums, self.k)
        expected = [3, 3, 5, 5, 6, 7]
        self.assertListEqual(res, expected)
